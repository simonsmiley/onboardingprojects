//
//  ViewController.swift
//  Weather
//
//  Created by Simon Smiley-Andrews on 22/7/21.
//

import UIKit

enum Person {
    case simon
    case brooke
    case jess
    
    var title: String {
        switch self {
        case .simon: return "Simon"
        case .brooke: return "Brooke"
        case .jess: return "Jess"
        }
    }
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private let people: [Person] = [.simon, .brooke, .jess]
    
    @IBOutlet private var tableView: UITableView! {
        didSet {
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
            tableView.tableFooterView = UIView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Hello world"
    }


    // MARK: - UITableViewDataSource, UITableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") else { return UITableViewCell(frame: .zero) }
        cell.textLabel?.text = people[indexPath.row].title
        cell.accessoryType = .disclosureIndicator
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

